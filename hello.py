from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.python_operator import BranchPythonOperator
from airflow.operators.http_operator import SimpleHttpOperator
from airflow.models import Variable
from datetime import datetime, timedelta
import json

args = {
    'owner': 'Airflow',
    'start_date': '2021-01-01',
    'retries': 1,
}

def print_hello():
    return 'Hello world from first Airflow DAG!'

def print_cold():
    return 'COLD!'

def print_hot():
    return 'HOT!'

dag = DAG(
    dag_id='FIRST_DAG',
    default_args=args,
    schedule_interval='@daily',
    catchup=False
)



def handle_response(response):
    if response.status_code == 200:
        print("OK")
        print("temperature: " + response.json().get("temperature"))
        print("description: " + response.json().get("description"))
        Variable.set("status", response.json().get("description"))
        return True
    else:
        print("ERROR")
        return False


def check():
    status = Variable.get("status")
    print(status)
    if status == 'Sunny':
        return "hot_task"
    else:
        return "cold_task"

hello_task = PythonOperator(task_id='hello_task', python_callable=print_hello, dag=dag)

cold_task = PythonOperator(task_id='cold_task', python_callable=print_cold, dag=dag)
hot_task = PythonOperator(task_id='hot_task', python_callable=print_hot, dag=dag)

weather_id = SimpleHttpOperator(
    task_id='weather_id',
    method='GET',
    endpoint='weather/Moscow',
    http_conn_id='weather_api',
    headers={"Content-Type": "application/json"},
    response_check=lambda response: handle_response(response),
    dag=dag)
    
    
check_task = BranchPythonOperator(task_id='check_task', python_callable=check, dag=dag)


hello_task >> weather_id >> check_task >> [cold_task, hot_task]
